# Test AudioKit

* Runs on iOS 9.0 and above.
* Targets Swift.
* EMBEDDED_CONTENT_CONTAINS_SWIFT should be set to YES on the Build Settings. See http://stackoverflow.com/a/26949219/758990.
* Provides easy waves visualization.
* Difficult to record sound.

## Recording

The recording feature was [requested](https://github.com/audiokit/AudioKit/issues/285) and
[added](https://github.com/audiokit/AudioKit/commit/e71de42823aa95868a07b78b205154d7729db1c1).
But in neither the [latest CocoaPods version (3.1.3)](https://github.com/audiokit/AudioKit/blob/v3.1.3/AudioKit/Common/Internals/AKNodeRecorder.swift),
nor the [development branch](https://github.com/audiokit/AudioKit/blob/develop/AudioKit/Common/Internals/AKNodeRecorder.swift) can it be used from Objective-C code.
It's not inherited from `NSObject` and not marked with `@objc`, nor is it inhereted from the
`AKNode` class. Other nodes do inherit from `AKNode`.