//
//  ViewController.m
//  Audio Proccessing Test
//
//  Created by Stanislav Sidelnikov on 21/07/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

#import "ViewController.h"

@import AudioKit;

@interface ViewController ()

@property (weak, nonatomic) IBOutlet EZAudioPlot *audioInputPlot;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (nonatomic, strong) AKMicrophone *mic;
@property (nonatomic, strong) AKFrequencyTracker *tracker;
@property (nonatomic, strong) AKBooster *silence;
@property (nonatomic, strong) AKNodeOutputPlot *plot;

@property (nonatomic) BOOL isRecording;

- (void)setupPlot;

@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.mic = [[AKMicrophone alloc] init];
  self.tracker = [[AKFrequencyTracker alloc] init:self.mic minimumFrequency:200 maximumFrequency:2000];
  self.silence = [[AKBooster alloc] init:self.tracker gain:0];

  self.isRecording = NO;
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];

  [AudioKit stop];
}

- (void)setupPlot {
  if (self.plot) {
    [self.plot clear];
  } else {
    self.plot = [[AKNodeOutputPlot alloc] init:self.mic frame:self.audioInputPlot.bounds bufferSize:20];
    self.plot.plotType = EZPlotTypeRolling;
    self.plot.shouldFill = YES;
    self.plot.shouldMirror = YES;
    self.plot.color = [UIColor blueColor];
    [self.audioInputPlot addSubview:self.plot];
  }
}

- (IBAction)recordButtonPressed:(UIButton *)sender {
  if (self.isRecording) {
    [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
    [AudioKit stop];
  } else {
    [self.recordButton setTitle:@"Stop" forState:UIControlStateNormal];

    [AudioKit setOutput:self.silence];
    [self setupPlot];
    [AudioKit start];
  }
  self.isRecording = !self.isRecording;
}

- (IBAction)playButtonPressed:(UIButton *)sender {
}


@end
